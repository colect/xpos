<!-- jQuery  -->
<script>
    var resizefunc = [];
</script>

<script src='/dashboard/js/jquery.min.js'></script>
<script src='/dashboard/js/bootstrap.min.js'></script>
<script src='/dashboard/js/detect.js'></script>
<script src='/dashboard/js/fastclick.js'></script>
<script src='/dashboard/js/jquery.slimscroll.js'></script>
<script src='/dashboard/js/jquery.blockUI.js'></script>
<script src='/dashboard/js/waves.js'></script>
<script src='/dashboard/js/wow.min.js'></script>
<script src='/dashboard/js/jquery.nicescroll.js'></script>
<script src='/dashboard/js/jquery.scrollTo.min.js'></script>


<script src='/dashboard/js/jquery.core.js'></script>
<script src='/dashboard/js/jquery.app.js'></script>

{{--Notification plugin--}}
<script src='/dashboard/plugins/notifyjs/js/notify.js'></script>
<script src='/dashboard/plugins/notifications/notify-metro.js'></script>

{{--form plugin--}}
<script src='/dashboard/js/jquery.uploadPreview.min.js'></script>
<script src='/dashboard/plugins/parsleyjs/parsley.min.js'></script>

<!-- Sweet-Alert  -->
<script src='/dashboard/plugins/bootstrap-sweetalert/sweet-alert.min.js'></script>

<script src='/dashboard/js/dashboard.js'></script>
<script>
    $(document).ready(function () {
        $("#loading").hide();
    })
</script>

