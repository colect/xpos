@extends('layouts.app')

@section('content')

    <div class="container">
        <h2>Welcome to POSx Installation process</h2>
        <p>Please follow the steps to complete the process</p>
        @if(session('step_error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Error!</strong> {{session('step_error')}}.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="row">
             @yield('content')
        </div>

    </div>
@endsection
